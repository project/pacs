The pacs Module (Tree Access Module)
====================================

Under pacs, every drupal node turns in a container wich can hold normal content
but also other nodes of any type, much like a directory in an apache web server.

You can control the access permissions on each of the nodes for each single
defined user role. Nodes with no explicit permission settings inherit them from
its parent node, like in a directory tree. 

This means that if you set permissions on one node you are actually setting
these permissions on a whole branch, composed by this node itself and all of his
descendants (child nodes), up to the next one with its own permission settings.

Pacs should work with any type of nodes.

The Node View
=============

Under pacs, when viewing a node, the page breadcrumb will show the position of
the node in the pacs tree instead of the normal menu items sequence (not for
book pages).

If you have the "view tree" system permission, there will be a new "tree" tab.
Clicking on this tab will open a nice representation of the tree structure.

If the current node has child nodes, their titles will show up as a list of page
footer links (not for book pages).

If you have the appropriate permissions, the page footer will also show a "new
node" link that lets you create a new node of any type right under the current
node. Clicking on the standard "create content" menu item lets you create a new
node under the tree root.

The Tree View
=============

Clicking on the pacs "tree" tab will open a representation of the tree
structure, where the permissions are represented by the color of each node. If
you have sufficient permission, you can set node permissions for each node/role
or move the nodes in the tree structure.

Pacs and Book Pages
===================

Book pages, the nodes provided by the Book Module, have their own hierarchical
structure. To avoid possible inconsistencies between the pacs and the book
hierarchies, pacs maintains them both automatically synchronized. So, if you
extend or update your book structure, pacs will update its tree accordingly.
This feature is optional.

The Access Permissions
======================

You can set permissions for each user role on any node in the tree. If no access
permission is set on a node, it inherits the permissions from the parent node.
So, access permissions propagate downwards the tree up to the next node wich
have its own permission definition for a particular role.

The possible permissions are 'none' (no access at all), 'view' (user can view
the node), 'update' (user can view and update the node) and 'manage' (user can
view, update or manage the node).

The 'manage' access permission is a pacs extension for the standard drupal
'delete' permission. With this permission, the user can view, update and delete
nodes, but also create new nodes, move them in the tree and set its access
permissions for all roles.

The system administrator user (uid = 1) has all permissions inconditionally.

Installation
============

1) Add the new pacs fields to the drupal 'node' table and drop the 'all' realm
   entry in the 'node_access' table:

   From the command line, type:
   mysql -u username -p drupal < pacs.mysql
   (where drupal is your drupal database and username is your mysql username)

2) Create a 'pacs' directory under your drupal installation's 'module'
   directory

3) Copy the files 'pacs.module', 'pacs.css' and 'help.css' into this directory

4) Activate the module from administer >> modules

Configuration
=============

1) From 'administer' >> 'settings' >> 'pacs', enable (recommended) or disable
   the book page synchronization.

2) If enabled, and if you already have book page hierarchies, click on
   'synchronize now' to get pacs synchronized with your book pages.

3) From 'administer' >> 'access control', enable the 'view tree' system
   permission for all roles that should be able to see the tree view, and 'manage
   tree' for all roles that should be able to set access permisions and change the
   tree structure.

Using pacs
==========

1) From any node view, click on the 'tree' tab to see and manage your new pacs
   node tree.

2) At the 'tree' view, click on the 'help' button for instructions.

That's it. Enjoy.
