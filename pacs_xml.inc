<?php

/**
 * @file
 * import.inc - the xml branch import include.
 */
 
$_PACS_XML_TAG = '';
$_PACS_XML_NODE = stdClass;
$_PACS_XML_NIDS = array();


// xml import functions ////////////////////////////////////////////////


function pacs_xml_save_node() {
  global $user, $_PACS_XML_NODE, $_PACS_XML_NIDS;

  $_PACS_XML_NODE->uid = $user->uid;
  $_PACS_XML_NODE->format = 1;
  node_save($_PACS_XML_NODE);
  //pacs_debug('saved_node='.print_r($_PACS_XML_NODE,1),1);
  
  $_PACS_XML_NIDS[$_PACS_XML_NODE->pacs_nid] = $_PACS_XML_NODE->nid;
  if ($_PACS_XML_NODE->pacs_index == 0) $_PACS_XML_NIDS[0] = $_PACS_XML_NODE->nid;
  $pid = $_PACS_XML_NIDS[$_PACS_XML_NODE->pacs_pid];
  if (!$pid) $pid = 0;
  $pnode = array('nid' => $_PACS_XML_NODE->nid, 'pid' => $pid, 'rid' => 0, 'stops' => array());
  pacs_update_node($pnode);
  if ($_PACS_XML_NODE->type == 'book') {
    // if the node is a book page, update the book hierarchy
    db_query("REPLACE {book} SET vid = %d, nid = %d, parent = %d", 
      $_PACS_XML_NODE->vid, $_PACS_XML_NODE->nid, $pid);
  }
}

function pacs_xml_start_element($parser,$name,$attrs) {
  global $_PACS_XML_TAG, $_PACS_XML_NODE;
  
  switch ($name) {
    case 'NODE':
      $_PACS_XML_NODE = (object) NULL;
      $_PACS_XML_NODE->pacs_nid = $attrs['NID'];
      break;
    case 'DRUPAL_VERSION':
    case 'TYPE':
    case 'STATUS':
    case 'PROMOTE':
    case 'MODERATE':
    case 'STICKY':
    case 'TITLE':
    case 'TEASER':
    case 'BODY':
    case 'LOG':
    case 'PACS_INDEX':
    case 'PACS_PID':
      $_PACS_XML_TAG = $name;
      break;
  }
}

function pacs_xml_element_content($parser,$data) {
  global $_PACS_XML_TAG, $_PACS_XML_NODE;
  
  switch ($_PACS_XML_TAG) {
    /*
    case 'DRUPAL_VERSION':
      if ($data != '4.7') {
        drupal_set_message(t('Incompatible Drupal version.'),'error');
        drupal_goto("node/$nid/tree");
      }
      break;
    */
    case 'TYPE':
      $_PACS_XML_NODE->type = $data;
      break;
    case 'STATUS':
      $_PACS_XML_NODE->status = $data;
      break;
    case 'PROMOTE':
      $_PACS_XML_NODE->promote = $data;
      break;
    case 'MODERATE':
      $_PACS_XML_NODE->moderate = $data;
      break;
    case 'STICKY':
      $_PACS_XML_NODE->sticky = $data;
      break;
    case 'TITLE':
      $_PACS_XML_NODE->title .= $data;
      break;
    case 'TEASER':
      $_PACS_XML_NODE->teaser .= $data;
      break;
    case 'BODY':
      $_PACS_XML_NODE->body .= $data;
      break;
    case 'LOG':
      $_PACS_XML_NODE->log .= $data;
      break;
    case 'PACS_INDEX':
      $_PACS_XML_NODE->pacs_index = $data;
      break;
    case 'PACS_PID':
      $_PACS_XML_NODE->pacs_pid = $data;
      break;
  }
}

function pacs_xml_end_element($parser,$name) {
  global $_PACS_XML_TAG, $_PACS_XML_NODE;
  
  if ($_PACS_XML_TAG == $name) $_PACS_XML_TAG = '';
  if ($name == 'NODE') pacs_xml_save_node();
}

function pacs_xml_parse($nid) {
  global $_PACS_XML_NIDS;

  $file = $_FILES['branch_file']['tmp_name'];
  if (!$file || !is_uploaded_file($file)) {
    drupal_set_message(t('Branch file upload failed.'),'error');
    drupal_goto("node/$nid/tree");
  }
  $xml = xml_parser_create();
  xml_parser_set_option ($xml,XML_OPTION_SKIP_WHITE,1);
  xml_set_element_handler($xml,'pacs_xml_start_element','pacs_xml_end_element');
  xml_set_character_data_handler($xml,'pacs_xml_element_content');
  $fp = fopen($file,"r");
  if (!$fp) {
    drupal_set_message(t('Could not open uploaded branch file.'),'error');
    drupal_goto("node/$nid/tree");
  }
  while ($data = fread($fp,4096)) {
    if (!xml_parse($xml, $data, feof($fp))) {
      $err_str = xml_error_string(xml_get_error_code($xml));
      $err_num = xml_get_current_line_number($xml);
      $err_msg = t('XML error: %s at line %d', array('%s' => $err_str, '%d' => $err_num));
      drupal_set_message($err_msg,'error');
      drupal_goto("node/$nid/tree");
    }
  }
  xml_parser_free($xml);
  pacs_move_node($_PACS_XML_NIDS[0],$nid,FALSE);
  drupal_goto("node/$nid/tree");
}

function pacs_xml_import($nid) {
  $output = '';

  if ($_POST['op_cancel']) drupal_goto("node/$nid/tree");
  if (isset($_FILES['branch_file'])) pacs_xml_parse($nid);
  
  // file upload form
  $form['#attributes'] = array('enctype'=>'multipart/form-data');
  $form['import'] = array('#type'=>'fieldset','#title'=>t('Pacs Branch Import'),
    '#description'=>t('Select a valid pacs export file to upload:'));
  $form['import']['file'] = array('#type'=>'file','#name'=>'branch_file','#size'=>'45',);
  $form['import']['cancel'] = array('#prefix'=>'<div>','#type'=>'submit',
    '#name'=>'op_cancel','#value'=>t('cancel'),);
  $form['import']['upload'] = array('#type'=>'submit','#value'=>t('upload'),'#suffix'=>'</div>',);
  $output .= drupal_get_form('xml_import',$form);
  print theme('page',$output);
}


// xml export functions ////////////////////////////////////////////////


function pacs_xml_export_node($nid) {
  static $node_index = 0;

  if ($node = node_load($nid,NULL,TRUE)) {
    print "<node nid=\"$nid\">\n";
    print "<type>".$node->type."</type>\n";
    print "<status>".$node->status."</status>\n";
    print "<promote>".$node->promote."</promote>\n";
    print "<moderate>".$node->moderate."</moderate>\n";
    print "<sticky>".$node->sticky."</sticky>\n";
    print "<title><![CDATA[".$node->title."]]></title>\n";
    print "<teaser><![CDATA[".$node->teaser."]]></teaser>\n";
    print "<body><![CDATA[".$node->body."]]></body>\n";
    print "<log><![CDATA[".$node->log."]]></log>\n";
    print "<pacs_index>$node_index</pacs_index>\n";  $node_index++;
    print "<pacs_pid>".$node->pacs['pid']."</pacs_pid>\n";
    print "</node>\n";
  }
}

function pacs_xml_export_branch($nid) {
  pacs_xml_export_node($nid);
  if ($list = pacs_child_list($nid)) foreach ($list as $chd) pacs_xml_export_branch($chd);
}

function pacs_xml_export($nid) {
  $xml_file_name = 'drupal_branch_'.$nid.'.xml';
  drupal_set_header('Content-Type: application/octet-stream');
  drupal_set_header('Content-Disposition: attachment; filename='.$xml_file_name);
  
  print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
  print "<pacs_branch>\n";
  print "<drupal_version>4.7</drupal_version>\n";
  pacs_xml_export_branch($nid);
  print "</pacs_branch>\n";
  
  exit;
}


// delete branch ////////////////////////////////////////////////


function pacs_del_branch($nid) {
  node_delete($nid);
  if ($list = pacs_child_list($nid)) foreach ($list as $chd) pacs_del_branch($chd);
}

function pacs_delete_branch($nid) {
  $pnode = pacs_node($nid);
  if ($_POST['op_delete']) {
    pacs_del_branch($nid);
    drupal_goto('node/'.$pnode['pid'].'/tree');
  } elseif ($_POST['op_cancel']) {
    drupal_goto('node/'.$nid.'/tree');
  }
  drupal_set_message(
    t('Do you want to delete the branch under the node <strong>%title</strong> (inclusive) ?',
    array('%title'=>$pnode['title'])),'error'
  );
  $output = '';
  $form['cancel'] = array('#type'=>'submit','#name'=>'op_cancel','#value'=>t('cancel'));
  $form['delete'] = array('#type'=>'submit','#name'=>'op_delete','#value'=>t('delete branch'));
  $output .= drupal_get_form('delete_confirmation',$form);
  print theme('page',$output);
}

?>